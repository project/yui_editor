function render_editor(id, height, width) {
    if ($("#"+id).val() != null) {
    var myConfig = {
          height: height+'px',
          width: width+'px',
          animate: true,
          handleSubmit: true
    };
    myEditor = new YAHOO.widget.Editor(id, myConfig);
myEditor.render();
    }
};
